import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.JEditorPane;
import javax.swing.JTextField;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	private Color standardfarbe;
	private JTextField txtHier;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 384, 702);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.window);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 39, 369, 21);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern ");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_1.setBounds(10, 105, 414, 21);
		contentPane.add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("Rot");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Rot");
				Color c = Color.red;
				contentPane.setBackground(c);
			}
		});
		btnNewButton.setBounds(10, 137, 93, 33);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Gr\u00FCn");
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				System.out.println("Gr�n");
				Color c = Color.green;
				contentPane.setBackground(c);
			}
		});
		btnNewButton_1.setBounds(122, 137, 108, 33);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Blau\r\n");
		btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Blau");
				Color c = Color.blue;
				contentPane.setBackground(c);

			}
		});
		btnNewButton_2.setBounds(252, 137, 108, 33);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Gelb");
		btnNewButton_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Gelb");
				Color c = Color.yellow;
				contentPane.setBackground(c);

			}
		});
		btnNewButton_3.setBounds(10, 181, 93, 33);
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Standardfarbe");
		btnNewButton_4.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Standardfarbe");
				contentPane.setBackground(standardfarbe);

			}
		});
		btnNewButton_4.setBounds(122, 181, 108, 33);
		contentPane.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("Farbe w\u00E4hlen");
		btnNewButton_5.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Farbe w�hlen");
				Color color =
					       JColorChooser.showDialog(MainFrame.this, " w�hle Farbe", 
					Color.red
					);
				contentPane.setBackground(color);

			}
		});
		btnNewButton_5.setBounds(252, 181, 108, 33);
		contentPane.add(btnNewButton_5);
		
		JLabel lblNewLabel_2 = new JLabel("Aufgabe 2: Text formatieren");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_2.setBounds(10, 225, 424, 21);
		contentPane.add(lblNewLabel_2);
		
		JButton btnNewButton_6 = new JButton("Arial");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 12));
				
			}
		});
		btnNewButton_6.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_6.setBounds(10, 257, 93, 33);
		contentPane.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("Comic Sans MS");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btnNewButton_7.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_7.setBounds(122, 257, 116, 33);
		contentPane.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("Courier New");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnNewButton_8.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_8.setBounds(252, 257, 108, 33);
		contentPane.add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("Ins Label schreiben");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = txtHier.getText();
				lblNewLabel.setText(text);
			}
		});
		btnNewButton_9.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_9.setBounds(10, 335, 174, 33);
		contentPane.add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("Text im Label l\u00F6schen");
		btnNewButton_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("");
			}
		});
		btnNewButton_10.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_10.setBounds(194, 335, 166, 33);
		contentPane.add(btnNewButton_10);
		
		JLabel lblNewLabel_3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_3.setBounds(10, 379, 350, 21);
		contentPane.add(lblNewLabel_3);
		
		JButton btnNewButton_11 = new JButton("Rot");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.red);
				
			}
		});
		btnNewButton_11.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_11.setBounds(10, 414, 93, 24);
		contentPane.add(btnNewButton_11);
		
		JButton btnNewButton_12 = new JButton("Blau");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.blue);
			}
		});
		btnNewButton_12.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_12.setBounds(122, 414, 108, 24);
		contentPane.add(btnNewButton_12);
		
		JButton btnNewButton_13 = new JButton("Schwarz");
		btnNewButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.black);
			}
		});
		btnNewButton_13.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_13.setBounds(252, 415, 108, 23);
		contentPane.add(btnNewButton_13);
		
		JLabel lblNewLabel_4 = new JLabel("Aufgabe 4: Scriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_4.setBounds(10, 449, 350, 21);
		contentPane.add(lblNewLabel_4);
		
		JButton btnNewButton_14 = new JButton("+");
		btnNewButton_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font schrift = lblNewLabel.getFont();
				int zahl = schrift.getSize();
				Font groesser = schrift.deriveFont((float)zahl + 1);
				
				lblNewLabel.setFont(groesser);
				//lblNewLabel.setFont(label.getFont().deriveFont(18f);
			}
		});
		btnNewButton_14.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_14.setBounds(10, 481, 174, 33);
		contentPane.add(btnNewButton_14);
		
		JButton btnNewButton_15 = new JButton("-");
		btnNewButton_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font schrift = lblNewLabel.getFont();
				int zahl = schrift.getSize();
				Font groesser = schrift.deriveFont((float)zahl - 1);
				
				lblNewLabel.setFont(groesser);
			}
		});
		btnNewButton_15.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_15.setBounds(194, 481, 166, 33);
		contentPane.add(btnNewButton_15);
		
		JLabel lblNewLabel_5 = new JLabel("Aufgabe 5: Textausrichting");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_5.setBounds(10, 525, 350, 14);
		contentPane.add(lblNewLabel_5);
		
		JButton btnNewButton_16 = new JButton("linksb\u00FCndig");
		btnNewButton_16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(JLabel.LEFT);
			}
		});
		btnNewButton_16.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_16.setBounds(10, 550, 93, 23);
		contentPane.add(btnNewButton_16);
		
		JButton btnNewButton_17 = new JButton("zentriert");
		btnNewButton_17.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(JLabel.CENTER);
			}
		});
		btnNewButton_17.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_17.setBounds(122, 550, 108, 23);
		contentPane.add(btnNewButton_17);
		
		JButton btnNewButton_18 = new JButton("rechtsb\u00FCndig");
		btnNewButton_18.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(JLabel.RIGHT);
			}
		});
		btnNewButton_18.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_18.setBounds(252, 550, 108, 23);
		contentPane.add(btnNewButton_18);
		
		JLabel lblNewLabel_6 = new JLabel("Aufgabe 6: Textausrichtung");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_6.setBounds(10, 584, 350, 21);
		contentPane.add(lblNewLabel_6);
		
		JButton btnNewButton_19 = new JButton("EXIT");
		btnNewButton_19.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	        dispose();
			}
		});
		btnNewButton_19.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_19.setBounds(10, 605, 350, 74);
		contentPane.add(btnNewButton_19);
		
		txtHier = new JTextField();
		txtHier.setText("Hier bitte Text eingeben");
		txtHier.setBounds(10, 301, 369, 20);
		contentPane.add(txtHier);
		txtHier.setColumns(10);
		this.standardfarbe = contentPane.getBackground();
	}
}
